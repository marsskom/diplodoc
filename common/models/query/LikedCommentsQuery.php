<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\LikedComments]].
 *
 * @see \common\models\LikedComments
 */
class LikedCommentsQuery extends \yii\db\ActiveQuery
{
    /**
     * {@inheritdoc}
     * @return \common\models\LikedComments[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\LikedComments|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
