<?php

namespace common\models\query;

use common\models\Comment;

/**
 * This is the ActiveQuery class for [[\common\models\Comment]].
 *
 * @see \common\models\Comment
 */
class CommentQuery extends \yii\db\ActiveQuery
{
    /**
     * @return CommentQuery
     */
    public function today()
    {
        return $this->andWhere(['>=', Comment::tableName() . '.created_at', strtotime('today midnight')]);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\Comment[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\Comment|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
