<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%liked_comments}}".
 *
 * @property int $comment_id
 * @property int $user_id
 *
 * @property Comment $comment
 * @property User $user
 */
class LikedComments extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%liked_comments}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['comment_id', 'user_id'], 'required'],
            [['comment_id', 'user_id'], 'integer'],
            [['comment_id', 'user_id'], 'unique', 'targetAttribute' => ['comment_id', 'user_id']],
            [['comment_id'], 'exist', 'targetClass' => Comment::class, 'targetAttribute' => ['comment_id' => 'id']],
            [['user_id'], 'exist', 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'comment_id' => Yii::t('common', 'Comment ID'),
            'user_id' => Yii::t('common', 'User ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComment()
    {
        return $this->hasOne(Comment::class, ['id' => 'comment_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\query\LikedCommentsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\LikedCommentsQuery(get_called_class());
    }
}
