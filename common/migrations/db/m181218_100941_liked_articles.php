<?php

use yii\db\Migration;

/**
 * Class m181218_100941_liked_articles
 */
class m181218_100941_liked_articles extends Migration
{
    /**
     * @return bool|void
     */
    public function Up()
    {
        $this->createTable('{{%liked_articles}}', [            
            'user_id' => $this->integer()->notNull(),
            'article_id' => $this->integer()->notNull(),
        ]);

        $this->addPrimaryKey('user_article_pk', '{{%liked_articles}}', ['user_id', 'article_id']);
    }

    /**
     * @return bool|void
     */
    public function Down()
    {
        $this->dropTable('{{%liked_articles}}');
    }
}