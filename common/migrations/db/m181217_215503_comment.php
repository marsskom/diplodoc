<?php

use yii\db\Migration;

/**
 * Class m181217_215503_comment
 */
class m181217_215503_comment extends Migration
{
    /**
     * @return bool|void
     */
    public function Up()
    {
        $this->createTable('{{%comment}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'text' => $this->string(1024)->notNull(),
            'parent_id' => $this->integer(),
            'article_id' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey('fk_comment_user', '{{%comment}}', 'user_id', '{{%user}}', 'id', 'cascade', 'cascade');
        $this->addForeignKey('fk_comment_article', '{{%comment}}', 'article_id', '{{%article}}', 'id', 'cascade', 'cascade');
    }

    /**
     * @return bool|void
     */
    public function Down()
    {
        $this->dropForeignKey('fk_comment_user', '{{%comment}}');
        $this->dropForeignKey('fk_comment_article', '{{%comment}}');

        $this->dropTable('{{%comment}}');
    }
}
