<?php

use yii\db\Migration;

/**
 * Class m181218_095745_liked_comments
 */
class m181218_095745_liked_comments extends Migration
{
    /**
     * @return bool|void
     */
    public function Up()
    {
        $this->createTable('{{%liked_comments}}', [
            'comment_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
        ]);

        $this->addPrimaryKey('comment_user_pk', '{{%liked_comments}}', ['comment_id', 'user_id']);

        $this->addForeignKey('fk_liked_comments_user', '{{%liked_comments}}', 'user_id', '{{%user}}', 'id', 'cascade', 'cascade');
        $this->addForeignKey('fk_liked_comments_comment', '{{%liked_comments}}', 'comment_id', '{{%comment}}', 'id', 'cascade', 'cascade');
    }

    /**
     * @return bool|void
     */
    public function Down()
    {
        $this->dropTable('{{%liked_comments}}');
    }
}
