<?php

use yii\db\Migration;

/**
 * Class m181218_102113_favorite_articles
 */
class m181218_102113_favorite_articles extends Migration
{
     /**
     * @return bool|void
     */
    public function Up()
    {
        $this->createTable('{{%favorite_articles}}', [            
            'user_id' => $this->integer()->notNull(),
            'article_id' => $this->integer()->notNull(),
        ]);

        $this->addPrimaryKey('user_article_pk', '{{%favorite_articles}}', ['user_id', 'article_id']);
    }

    /**
     * @return bool|void
     */
    public function Down()
    {
        $this->dropTable('{{%favorite_articles}}');
    }
}
