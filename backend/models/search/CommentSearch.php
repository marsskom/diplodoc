<?php

namespace backend\models\search;

use common\models\User;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Comment;

/**
 * Class CommentSearch
 * @package backend\models\search
 */
class CommentSearch extends Comment
{
    public $username;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'article_id'], 'integer'],
            [['username', 'text', 'created_at'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Comment::find()->joinWith(['user']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['created_at' => SORT_DESC],
                'attributes' => [
                    'created_at' => [
                        'asc' => [Comment::tableName() . '.created_at' => SORT_ASC],
                        'desc' => [Comment::tableName() . '.created_at' => SORT_DESC],
                        'default' => SORT_ASC,
                    ],
                    'username' => [
                        'asc' => [User::tableName() . '.username' => SORT_ASC],
                        'desc' => [User::tableName() . '.username' => SORT_DESC],
                        'default' => SORT_ASC,
                    ],
                ],
            ],
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'article_id' => $this->article_id,
        ]);

        $query->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', User::tableName() . '.username', $this->username]);

        return $dataProvider;
    }

    /**
     * @return int|string
     */
    public static function getTodayCount()
    {
        return Comment::find()->today()->count();
    }
}
